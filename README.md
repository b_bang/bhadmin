</div>
<h3 align="center">
<span style="color: #FF0000">B</span><span style="color: #FF0000">h</span>
<span style="color: #FF0000">A</span><span style="color: #FF0000">d</span><span style="color: #FF0000">m</span><span style="color: #FF0000">i</span><span style="color: #FF0000">n</span>
</h3>
<div align="center">

[![star](https://gitee.com/zhc02/bhdmin/badge/star.svg?theme=dark)](https://gitee.com/zhc02/bhdmin/stargazers)
[![fork](https://gitee.com/zhc02/bhdmin/badge/fork.svg?theme=dark)](https://gitee.com/zhc02/bhdmin/badge/fork.svg?theme=dark)


</div>


> 运行环境要求PHP8.0+。

## 主要特性

### 开源无加密
源码开源无加密，有详细的代码注释，有完整系统手册
### webman框架
一样的写法，十倍的性能  超高性能可扩展PHP框架
### 前端采用Vue CLI框架
前端使用Vue CLI框架nodejs打包，页面加载更流畅，用户体验更好
### 标准接口
标准接口、前后端分离，二次开发更方便
### 支持队列
降低流量高峰，解除耦合，高可用
### 长连接
减少CPU及内存使用及网络堵塞，减少请求响应时长
### 无缝事件机制
行为扩展更方便，方便二次开发
### 数据表格导出
PHPExcel数据导出,导出表格更加美观可视；
### 数据统计分析
后台使用ECharts图表统计，实现用户、产品、订单、资金等统计分析
### 强大的后台权限管理
后台多种角色、多重身份权限管理，权限可以控制到每一步操作
## 内容展示
![输入图片说明](data/acreenshoot/1.png)
![输入图片说明](data/acreenshoot/2.png)
![输入图片说明](data/acreenshoot/3.png)
![输入图片说明](data/acreenshoot/4.png)

## 安装

## 手动安装
1.创建数据库，倒入数据库文件

数据库文件目录/data/bhadmin.sql
安装redis

2.修改数据库连接文件
配置文件路径/.env

3.启动：

windows: php windows.php start

linux: php start.php start

参数
- --d : 后台执行


4.后台登录：
http://127.0.0.1:7777/admin

默认账号：admin 密码：123456



## 文档

[使用手册](http://help.bhadmin.cn)
[webman开发手册](https://www.workerman.net/doc/webman/)

## 交流群
点击链接加入群聊【Bhadmin】：http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=T_LVO2njvCIBtcHGYIVkKKZTVl3VxcoR&authKey=Q7ZP9wV6DCBatTzux8C5d3QrUTUDEalzgTv68mGCp4sZ8EFZlfPxxT1c7NX%2BGvHw&noverify=0&group_code=914198812
## 版权信息


本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2017-2024 by BhAdmin (http://www.bhadmin.cn)

All rights reserved。

