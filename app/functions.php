<?php
/**
 * Here is your custom functions.
 */


if (!function_exists('checkenv')) {
    /**
     * 检测环境变量
     */
    function checkenv()
    {
        $items = [];
        $items['php'] = PHP_VERSION;
        $items['mysqli'] = extension_loaded('mysqli');
        $items['redis'] = extension_loaded('redis');
        $items['curl'] = extension_loaded('curl');
        $items['fileinfo'] = extension_loaded('fileinfo');
        $items['exif'] = extension_loaded('exif');
        $items['gd'] = extension_loaded('gd');

        return $items;
    }
}

if (!function_exists('success')) {
    /**
     * api返回
     */
    function success($data = [])
    {
        return \support\Response::success($data);
    }
}

if (!function_exists('message')) {

    /**
     * 消息数组函数
     * @param string $msg 提示语
     * @param bool $success 是否成功
     * @param array $data 结果数据
     * @param int $code 错误码
     * @return array 返回消息对象
     */
    function message($data = [], $code = 0, $msg = "操作成功")
    {
        $result['code'] = $code;
        $result ['msg'] = $msg;
        $result['data'] = $data;
        return $result;
    }
}

if (!function_exists('error')) {
    /**
     * 错误返回json
     * @param $code
     * @param $message
     * @param $data
     * @return \support\Response
     */
    function error($code, $message, $data = [], $track = [])
    {
        $return_data['code'] = $code ?: -1;
        $return_data['msg'] = $message ?: 'Server internal error';
        $return_data['data'] = $data;
        $return_data['track'] = $track;
        return json($return_data);
    }
}

if (!function_exists('array_key_value')) {

    /**
     * 获取数组中某个字段的所有值
     * @param array $arr 数据源
     * @param string $name 字段名
     * @return array 返回结果
     */
    function array_key_value($arr, $name = "")
    {
        $result = [];
        if ($arr) {
            foreach ($arr as $key => $val) {
                if ($name) {
                    $result[] = $val[$name];
                } else {
                    $result[] = $key;
                }
            }
        }
        $result = array_unique($result);
        return $result;
    }
}

if (!function_exists('toArray')) {

    function toArray($obj)
    {

        return json_decode(json_encode($obj), true);
    }
}

if (!function_exists('get_password')) {

    /**
     * 获取双MD5加密密码
     * @param string $password 加密字符串
     * @return string 返回结果
     * @date 2019/4/5
     */
    function get_password($password)
    {
        return md5(md5($password));
    }
}
if (!function_exists('getConfig')) {
    function getConfig($code)
    {
        if (!$code) return [];
        $config = \support\Model\ConfigModel::where('code', $code)->first();
        if (!$config) return [];
        $config_data = \support\Model\ConfigDataModel::where('config_id', $config->id)->pluck('value', 'code');
        return toArray($config_data);
    }
}
if (!function_exists('getConfigData')) {
    function getConfigData($code)
    {
        if (!$code) return [];
        $value = \support\Model\ConfigDataModel::where('code', $code)->value('value');
        return $value ?? '';
    }
}

if (!function_exists('getConfByType')) {
    function getConfByType($type)
    {
        $config['store_way'] = 'local';
        return $config;
    }
}
