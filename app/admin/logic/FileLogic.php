<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\logic;

use support\Service\FileService;

class FileLogic  extends BaseLogic
{
    public static function add($data = [])
    {
        $save['name'] = $data['name'] ?? '';
        $save['length'] = $data['length'] ?? 0;
        $save['url'] = $data['url'] ?? '';
        $save['directory'] = $data['directory'] ?? 1;
        $save['type'] = $data['type'] ?? 1;
        $save['pid'] = $data['pid'] ?? 0;
        $save['sort'] = $data['sort'] ?? 0;
        $save['mime_type'] = $data['mime_type'] ?? '';
        $save['extension'] = $data['extension'] ?? '';
        $save['adapter'] = $data['adapter'] ?? 'local';
        return message(FileService::add($save));

    }
}
