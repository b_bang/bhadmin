<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\AdService;

class AdLogic extends BaseLogic
{

    public function __construct()
    {
        self::$service = new AdService();
    }

    public static function getList()
    {
        $param = request()->get();
        $where[] = ["mark", '=', 1];
        if (isset($param['name']) && $param['name']) {
            $where[] = ["name", '=', $param['name']];
        }
        if (isset($param['create_time']) && $param['create_time']) {
            $where[] = ["create_time", '>=', strtotime($param['create_time'][0])];
            $where[] = ["create_time", '<=', strtotime($param['create_time'][1])];
        }
        $list = self::$service::getListPage($where);
        if ($list['total'] <= 0) return message($list);
        return message($list);
    }

    public static function edit()
    {

        $data = request()->post();
        $id = $data['id'] ?? 0;
        if (!isset($data['name']) || empty($data['name'])) {
            throw new ApiException('请输入广告标题');
        }
        if (!isset($data['url']) || empty($data['url'])) {
            throw new ApiException('请输入链接地址');
        }
        if (!isset($data['device']) || empty($data['device'])) {
            throw new ApiException('请选择投放设备');
        }
        if (!isset($data['cover']) || empty($data['cover'])) {
            throw new ApiException('请上传广告图片');
        }
        if (!isset($data['url']) || empty($data['url'])) {
            throw new ApiException('请输入链接地址');
        }
        if (!isset($data['sort']) || empty($data['sort'])) {
            throw new ApiException('请输入排序');
        }
        if (!isset($data['time_range']) || empty($data['time_range'])) {
            throw new ApiException('请选择展示时间');
        }

        $save['name'] = trim($data['name']);
        $save['url'] = $data['url'];
        if (in_array('1', $data['device'])) {
            $save['device'] = '1';
        } else {
            $save['device'] = implode(',', $data['device']);
        }
        $save['cover'] = $data['cover'];
        $save['sort'] = $data['sort'];
        $save['start_time'] = strtotime($data['time_range'][0]);
        $save['end_time'] = strtotime($data['time_range'][1]);
        $save['status'] = $data['status'] ?? 1;
        $save['content'] = $data['content'] ?? '';
        unset($data['id']);
        if ($id == 0) {
            self::$service::add($save);
        } else {
            self::$service::save($save, $id);
        }
        return message();

    }


}
