<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\DeptService;

class DeptLogic  extends BaseLogic
{

    public static function getList()
    {
        $param = request()->get();
        $where[] = ["mark", '=', 1];
        if (isset($param['name']) && $param['name']) {
            $where[] = ["name", 'like', "%{$param['name']}%"];
        }
        $list = DeptService::getList($where);
        return message($list);
    }

    /**
     * 获取部门列表
     * @return array
     * @since 2021/5/26
     */
    public static function getDeptList()
    {
        $list = DeptService::getDeptList();
        return message($list);
    }


    public static function edit()
    {

        $data = request()->post();
        $id = $data['id'] ?? 0;
        if (!isset($data['name']) || empty($data['name'])) {
            throw new ApiException('请输入部门名称');
        }
        if (!isset($data['fullname']) || empty($data['fullname'])) {
            throw new ApiException('请输入部门全称');
        }
        if (!isset($data['code']) || empty($data['code'])) {
            throw new ApiException('请输入部门编码');
        }
        if (!isset($data['type']) || empty($data['type'])) {
            throw new ApiException('请选择部门类型');
        }
        if (!isset($data['sort']) || empty($data['sort'])) {
            throw new ApiException('请输入排序');
        }
        $save['name'] = trim($data['name']);
        $save['sort'] = $data['sort'];
        $save['fullname'] = trim($data['fullname']);
        $save['code'] = $data['code'];
        $save['pid'] = $data['pid'] ?? 0;
        $save['type'] = $data['type'];
        $save['note'] = $data['note'] ?? '';
        unset($data['id']);
        if ($id == 0) {
            DeptService::add($save);
        } else {
            DeptService::save($save, $id);
        }
        return message();

    }

    public static function delete()
    {
        $ids = request()->post('id');
        if (!$ids) throw new ApiException('记录ID不能为空');
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        DeptService::delete($ids);
        return message();
    }

    public static function status()
    {
        $id = request()->post('id');
        $status = request()->post('status');
        if (!$id) throw new ApiException('记录ID不能为空');
        if (!in_array($status, [1, 2])) throw new ApiException('状态错误');
        DeptService::save(['status' => $status], $id);
        return message();
    }

}
