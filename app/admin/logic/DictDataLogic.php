<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\DictDataService;

class DictDataLogic extends BaseLogic
{
    /**
     * 分页列表
     * @return array
     */
    public static function getList()
    {
        $param = request()->all();
        if (!isset($param['dictId'])) {
            return message();
        }
        $where = [];
        $where[] = ["mark", '=', 1];
        $where[] = ["dict_id", '=', (int)$param['dictId']];
        if (isset($param['name']) && $param['name']) {
            $where[] = ["name", 'like', "%{$param['name']}%"];
        }
        if (isset($param['code']) && $param['code']) {
            $where[] = ["code", 'like', "%{$param['code']}%"];
        }
        $list = DictDataService::getListPage($where, ['*'], ['sort' => 'asc'], [], $param['limit'] ?? 10);
        return message($list);
    }

    public static function edit()
    {

        $data = request()->post();
        $id = $data['id'] ?? 0;
        if (!isset($data['dict_id']) || empty($data['dict_id'])) {
            throw new ApiException('请选择字典分类');
        }
        if (!isset($data['name']) || empty($data['name'])) {
            throw new ApiException('请输入字典项标题');
        }

        if (!isset($data['code']) || empty($data['code'])) {
            throw new ApiException('请输入字典项值');
        }
        if (!isset($data['sort']) || empty($data['sort'])) {
            throw new ApiException('请输入排序');
        }
        $save['dict_id'] = $data['dict_id'];
        $save['name'] = trim($data['name']);
        $save['sort'] = $data['sort'];
        $save['code'] = $data['code'] ?? '';
        $save['note'] = $data['note'] ?? '';
        unset($data['id']);
        if ($id == 0) {
            $count = DictDataService::getUserCountByWhere([["code", '=', $save['code']], ["mark", "=", 1]]);
            if ($count > 0) {
                throw new ApiException('系统中已存在相同的配置项值');
            }
            DictDataService::add($save);
        } else {
            $count = DictDataService::getUserCountByWhere([["code", '=', $save['code']], ["id", "<>", $id], ["mark", "=", 1]]);
            if ($count > 0) {
                throw new ApiException('系统中已存在相同的配置项值');
            }
            DictDataService::save($save, $id);
        }
        return message();

    }

    public static function delete()
    {
        $ids = request()->post('id');
        if (!$ids) throw new ApiException('记录ID不能为空');
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        DictDataService::delete($ids);
        return message();
    }

    public static function status()
    {
        $id = request()->post('id');
        $status = request()->post('status');
        if (!$id) throw new ApiException('记录ID不能为空');
        if (!in_array($status, [1, 2])) throw new ApiException('状态错误');
        DictDataService::save(['status' => $status], $id);
        return message();
    }


}
