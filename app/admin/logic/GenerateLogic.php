<?php

namespace app\admin\logic;

use support\Db;

class GenerateLogic  extends BaseLogic
{

    public static function getList()
    {
        // 查询SQL
        $sql = "SHOW TABLE STATUS WHERE 1";
        // 请求参数
        $param = request()->all();
        $name = $param['name'] ?? '';
        $comment = $param['comment'] ?? '';
        $limit = $param['limit'] ?? 10;
        $page = $param['page'] ?? 1;
        if ($name) {
            $sql .= " AND NAME like \"%{$name}%\" ";

        }
        if ($comment) {
            $sql .= " AND COMMENT like \"%{$comment}%\" ";
        }
        $list = Db::select($sql);
        $startIndex = ($page - 1) * $limit;
        // 切片操作，提取指定范围内的元素
        $data = array_slice(toArray($list), $startIndex, $limit);

        $data = array_map('array_change_key_case', $data);
        return array(
            "msg" => '操作成功',
            "code" => 0,
            "data" => $data,
            "count" => count($list),
        );
    }
}
