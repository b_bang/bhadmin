<?php

namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\UserExtractService;

class UserExtractLogic extends BaseLogic
{


    public function __construct()
    {
        self::$service = new UserExtractService();
    }

    public static function getList()
    {
        $param = request()->get();
        $where[] = ["mark", '=', 1];
        if (isset($param['member_id']) && $param['member_id']) {
            $where[] = ["member_id", '=', (int)$param['member_id']];
        }
        if (isset($param['name']) && $param['name']) {
            $where[] = ["name", '=', $param['name']];
        }
        if (isset($param['create_time']) && $param['create_time']) {
            $where[] = ["create_time", '>=', strtotime($param['create_time'][0])];
            $where[] = ["create_time", '<=', strtotime($param['create_time'][1])];
        }
        $list = self::$service::getListPage($where);
        if ($list['total'] <= 0) return message($list);
        foreach ($list['list'] as $k => $v) {
            $list['list'][$k]['extract_rate'] = bcadd(0, 0, 2);
            $list['list'][$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        return message($list);
    }

    public static function pass()
    {
        $id = request()->post('id');

        self::$service::save(['status' => 1, 'examine_time' => time()], $id);
        return message();
    }

    public static function refuse()
    {
        $id = request()->post('id');
        $fail_msg = trim(request()->post('fail_msg', ''));
        self::$service::save(['status' => -1, 'fail_msg' => $fail_msg, 'examine_time' => time()], $id);
        return message();
    }
}
