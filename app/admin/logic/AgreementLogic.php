<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\AgreementService;

class AgreementLogic extends BaseLogic
{

    public function __construct()
    {
        self::$service = new AgreementService();
    }


    public static function edit()
    {
        $data = request()->post();
        $id = $data['id'] ?? 0;
        if (!isset($data['title']) || empty($data['title'])) {
            throw new ApiException('请输入协议标题');
        }
        if (!isset($data['content']) || empty($data['content'])) {
            throw new ApiException('请输入协议内容');
        }
        $save['title'] = trim($data['title']);
        $save['content'] = $data['content'];
        unset($data['id']);
        if ($id == 0) {
            self::$service::add($save);
        } else {
            self::$service::save($save, $id);
        }
        return message();
    }

}
