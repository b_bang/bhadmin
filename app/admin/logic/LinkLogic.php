<?php

namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\LinkService;

class LinkLogic extends BaseLogic
{

    public function __construct()
    {
        self::$service = new LinkService();
    }

    /**
     * 分页列表
     * @return array
     */
    public static function getList()
    {
        $param = request()->all();
        $where = [];
        $where[] = ["mark", '=', 1];
        if (isset($param['name']) && $param['name']) {
            $where[] = ["name", 'like', "%{$param['name']}%"];
        }
        if (isset($param['type']) && $param['type']) {
            $where[] = ["type", '=', $param['type']];
        }
        if (isset($param['platform']) && $param['platform']) {
            $where[] = ["platform", '=', $param['platform']];
        }
        $list = self::$service::getListPage($where, ['*'], ['sort' => 'asc'], [], $param['limit'] ?? 10);
        return message($list);
    }

    public static function edit()
    {

        $data = request()->post();
        $id = $data['id'] ?? 0;

        if (!isset($data['name']) || empty($data['name'])) {
            throw new ApiException('请输入友链名称');
        }
        if (!isset($data['url']) || empty($data['url'])) {
            throw new ApiException('请输入友链URL');
        }
        if (!isset($data['sort']) || empty($data['sort'])) {
            throw new ApiException('请输入排序');
        }
        if (!isset($data['status']) || empty($data['status'])) {
            throw new ApiException('请选择友链状态');
        }
        $save['name'] = trim($data['name']);
        $save['url'] = $data['url'];
        $save['sort'] = $data['sort'];
        $save['status'] = $data['status'];
        $save['type'] = $data['type'] ?? 1;
        $save['form'] = $data['form'] ?? 1;
        $save['image'] = $data['image'] ?? '';
        $save['platform'] = $data['platform'] ?? 1;
        unset($data['id']);
        if ($id == 0) {
            self::$service::add($save);
        } else {
            self::$service::save($save, $id);
        }
        return message();

    }


}
