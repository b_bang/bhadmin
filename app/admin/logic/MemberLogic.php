<?php

namespace app\admin\logic;

use support\Db;
use support\Exception\ApiException;
use support\Model\MemberAccountModel;
use support\Model\MemberBalanceLog;
use support\Service\MemberAccountService;
use support\Service\MemberService;
use support\Service\MemberBalanceLogService;

class MemberLogic  extends BaseLogic
{
    public static function getList()
    {
        $param = request()->get();
        $where[] = ["mark", '=', 1];
        if (isset($param['id']) && $param['id']) {
            $where[] = ["id", '=', (int)$param['id']];
        }
        if (isset($param['username']) && $param['username']) {
            $where[] = ["username", 'like', "%{$param['username']}%"];
        }
        if (isset($param['nickname']) && $param['nickname']) {
            $where[] = ["nickname", 'like', "%{$param['nickname']}%"];
        }
        if (isset($param['phone']) && $param['phone']) {
            $where[] = ["phone", '=', $param['phone']];
        }
        if (isset($param['create_time']) && $param['create_time']) {
            $where[] = ["create_time", '>=', strtotime($param['create_time'][0])];
            $where[] = ["create_time", '<=', strtotime($param['create_time'][1])];
        }
        $list = MemberService::getListPage($where, ['*'], ['id' => 'desc'], ['memberAccount']);

        if ($list['total'] <= 0) return message($list);
        foreach ($list['list'] as $k => $item) {
            $item['balance'] = $item['member_account']['balance'] ?? 0;
            $item['integral'] = $item['member_account']['integral'] ?? 0;
            unset($item['member_account']);
            $list['list'][$k] = $item;
        }

        return message($list);
    }

    public static function edit()
    {

        $data = request()->post();
        $id = $data['id'] ?? 0;
        if (!isset($data['status']) || empty($data['status'])) {
            throw new ApiException('请选择状态');
        }
        $save['status'] = $data['status'] ?? 1;
        $save['nickname'] = $data['nickname'] ?? 1;
        unset($data['id']);
        if (!$id) {
            if (isset($data['password']) && $data['password']) {
                $save['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }

        } else {

        }
        if ($id == 0) {
            MemberService::add($save);
        } else {
            MemberService::save($save, $id);
        }
        return message();

    }

    public static function delete()
    {
        $ids = request()->post('id');
        if (!$ids) throw new ApiException('记录ID不能为空');
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        MemberService::delete($ids);
        return message();
    }

    public static function status()
    {
        $id = request()->post('id');
        $status = request()->post('status');
        if (!$id) throw new ApiException('记录ID不能为空');
        if (!in_array($status, [1, 2])) throw new ApiException('状态错误');
        MemberService::save(['status' => $status], $id);
        return message();
    }

    /**l
     * 更改余额
     * @param $param
     * @return array
     */
    public static function changeBalance()
    {
        $param = request()->post();
        if (!isset($param['user_id']) || empty($param['user_id'])) {
            throw new ApiException('请选择用户');
        }

        if (!isset($param['type']) || empty($param['type']) || !in_array($param['type'], [1, 2])) {
            throw new ApiException('类型错误');
        }
        if (!isset($param['change_balance']) || (int)$param['change_balance'] <= 0) {
            throw new ApiException('变动余额值应大于0');
        }
        MemberBalanceLogService::changeMemberMoney(
            $param['user_id'],
            ($param['type'] == 1) ? 3 : 4,
            $param['type'] == 1 ? 1 : 2,
            $param['change_balance'],
            '后台' . (($param['type'] == 1) ? '增加' : '减少') . '余额'
        );
        return message();
    }

}
