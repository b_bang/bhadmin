<?php

namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\LogService;

class LogLogic  extends BaseLogic
{
    public static function loginLogList()
    {
        $param = request()->get();
        $where[] = ['type', "<", 3];
        $where[] = ['mark', "=", 1];
        if (isset($param['username']) && $param['username']) {
            $where[] = ["username", 'like', "%{$param['username']}%"];
        }
        $list = LogService::getListPage($where, ['*'], ['id' => 'desc'], [], request()->get('limit', 10));
        return message($list);
    }

    public static function actionLogList()
    {
        $param = request()->get();
        $where[] = ['mark', "=", 1];
        $where[] = ['type', "=", 3];
        if (isset($param['username']) && $param['username']) {
            $where[] = ["username", 'like', "%{$param['username']}%"];
        }
        if (isset($param['model']) && $param['model']) {
            $where[] = ["module", '=', $param['model']];
        }
        $list = LogService::getListPage($where, ['*'], ['id' => 'desc'], [], request()->get('limit', 10));
        return message($list);
    }

    public static function delete()
    {
        $ids = request()->post('id');
        if (!$ids) throw new ApiException('记录ID不能为空');
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        LogService::delete($ids);
        return message();
    }
}
