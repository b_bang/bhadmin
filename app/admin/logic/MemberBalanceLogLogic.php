<?php

namespace app\admin\logic;

use support\Service\MemberBalanceLogService;

class MemberBalanceLogLogic  extends BaseLogic
{

    public function __construct()
    {
        self::$service = new MemberBalanceLogService();
    }
    public static function getList()
    {
        $param = request()->get();
        $where = [];
        $limit = $param['limit'] ?? 10;
        if (isset($param['member_id']) && $param['member_id']) {
            $where[] = ["member_id", '=', (int)$param['member_id']];
        }
        if (isset($param['create_time']) && $param['create_time']) {
            $where[] = ["create_time", '>=', strtotime($param['create_time'][0])];
            $where[] = ["create_time", '<=', strtotime($param['create_time'][1])];
        }
        $list =   self::$service::getListPage($where, ['*'], ['id' => 'desc'], [], $limit);

        if ($list['total'] <= 0) return message($list);
        foreach ($list['list'] as $k => $item) {
            $list['list'][$k]['create_time'] = date('Y-m-d H:i:s', $item['create_time']);
        }

        return message($list);
    }
}
