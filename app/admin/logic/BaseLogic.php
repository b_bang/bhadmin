<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------

namespace app\admin\logic;

use support\Exception\ApiException;

/**
 * 基础Logic
 */
class BaseLogic
{
    protected static $service = null;

    /**
     * 获取列表
     * @return array
     */
    public static function getList()
    {
        $param = request()->get();
        $where[] = ["mark", '=', 1];
        if (isset($param['username']) && $param['username']) {
            $where[] = ["username", 'like', "%{$param['username']}%"];
        }
        $list = self::$service::getListPage($where);
        return message($list);
    }

    /**
     * 获取详情
     * @return array
     */
    public static function info()
    {
        $id = request()->get('id', 0);
        if (!$id) throw new ApiException('记录不存在！');
        $info = self::$service::getInfo($id);
        if (!$info) throw new ApiException('记录不存在！');
        return message($info);
    }


    /**
     * 删除
     * @return array
     */
    public static function delete()
    {
        $ids = request()->post('id');
        if (!$ids) throw new ApiException('记录ID不能为空');
        if (!is_array($ids)) $ids = [$ids];
        self::$service::delete($ids);
        return message([], 0, '删除成功');
    }

    /**
     * 状态
     * @return array
     */
    public static function status()
    {
        $id = request()->post('id');
        $status = request()->post('status');
        if (!$id) throw new ApiException('记录ID不能为空');
        if (!in_array($status, [1, 2])) throw new ApiException('状态错误');
        self::$service::save(['status' => $status], $id);
        return message();
    }
}
