<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\CityService;
use support\Service\ConfigService;

class CityLogic extends BaseLogic
{
    /**
     * 分页列表
     * @return array
     */
    public static function getList()
    {
        $param = request()->all();

        $where = [];
        $where[] = ["mark", '=', 1];
        $pid = $param['pid'] ?? 0;
        $where[] = ["pid", '=', (int)$pid];
        if (isset($param['name']) && $param['name']) {
            $where[] = ["name", 'like', "%{$param['name']}%"];
        }
        $list = CityService::getList($where, ['*'], ['sort' => 'asc']);
        if (!$list) return message();
        foreach ($list as &$val) {
            if ($val['level'] <= 2) {
                $val['hasChildren'] = true;
            }
        }
        return message($list);
    }

    public static function edit()
    {

        $data = request()->post();
        $id = $data['id'] ?? 0;

        if (!isset($data['pid']) || empty($data['pid'])) {
            throw new ApiException('请选择上级');
        }
        if (!isset($data['name']) || empty($data['name'])) {
            throw new ApiException('请输入城市名称');
        }
        if (!isset($data['level']) || empty($data['level'])) {
            throw new ApiException('请选择城市等级');
        }

        if (!isset($data['citycode']) || empty($data['citycode'])) {
            throw new ApiException('请输入城市编码');
        }

        $save['name'] = trim($data['name']);
        $save['pid'] = $data['pid'];
        $save['level'] = $data['level'];
        $save['citycode'] = $data['citycode'];
        $save['adcode'] = $data['adcode'] ?? '';
        $save['lat'] = $data['lat'] ?? '';
        $save['lng'] = $data['lng'] ?? '';
        $save['sort'] = $data['lng'] ?? '';
        unset($data['id']);
        if ($id == 0) {
            $count = CityService::getUserCountByWhere([["citycode", '=', $save['citycode']], ["mark", "=", 1]]);
            if ($count > 0) {
                throw new ApiException('系统中已存在相同的城市编码');
            }
            CityService::add($save);
        } else {
            $count = CityService::getUserCountByWhere([["citycode", '=', $save['citycode']], ["id", "<>", $id], ["mark", "=", 1]]);
            if ($count > 0) {
                throw new ApiException('系统中已存在相同的城市编码');
            }
            CityService::save($save, $id);
        }
        return message();

    }

    public static function delete()
    {
        $ids = request()->post('id');
        if (!$ids) throw new ApiException('记录ID不能为空');
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        CityService::delete($ids);
        return message();
    }

    public static function status()
    {
        $id = request()->post('id');
        $status = request()->post('status');
        if (!$id) throw new ApiException('记录ID不能为空');
        if (!in_array($status, [1, 2])) throw new ApiException('状态错误');
        CityService::save(['status' => $status], $id);
        return message();
    }
}
