<?php

namespace app\admin\logic;

use support\Exception\ApiException;
use support\Service\PositionService;

class PositionLogic extends BaseLogic
{

    public static function getList()
    {
        $where = [];
        $where['mark'] = 1;
        $list = PositionService::getListPage($where);
        return message($list);
    }

    /**
     * 获取岗位列表
     * @return array
     * @since 2020/11/20
     */
    public static function getPositionList()
    {
        $list = PositionService::getPositionList();
        return message($list);
    }

    public static function edit()
    {

        $data = request()->post();
        $id = $data['id'] ?? 0;

        if (!isset($data['name']) || empty($data['name'])) {
            throw new ApiException('请输入岗位名称');
        }
        if (!isset($data['sort']) || empty($data['sort'])) {
            throw new ApiException('请输入排序');
        }
        if (!isset($data['status']) || empty($data['status'])) {
            throw new ApiException('请选择岗位状态');
        }
        $save['name'] = trim($data['name']);
        $save['sort'] = $data['sort'];
        $save['status'] = $data['status'];
        unset($data['id']);
        if ($id == 0) {
            PositionService::add($save);
        } else {
            PositionService::save($save, $id);
        }
        return message();

    }

    public static function delete()
    {
        $ids = request()->post('id');
        if (!$ids) throw new ApiException('记录ID不能为空');
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        PositionService::delete($ids);
        return message();
    }

    public static function status()
    {
        $id = request()->post('id');
        $status = request()->post('status');
        if (!$id) throw new ApiException('记录ID不能为空');
        if (!in_array($status, [1, 2])) throw new ApiException('状态错误');
        PositionService::save(['status' => $status], $id);
        return message();
    }
}
