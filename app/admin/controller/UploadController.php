<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\logic\FileLogic;
use support\Request;
use support\Strategy\Filesystem\Facade\Storage;
/**
 * 上传管理
 */
class UploadController extends Backend
{

    public function images()
    {
        $cateId = request()->input('cate_id', 0);
        $file = request()->file('file');
        $path = '/upload/img/' . date('Ymd');
        try {
            $result = Storage::path($path)->size(1024 * 1024 * 5)->extYes([])->upload($file);
            FileLogic::add([
                'adapter' => $result->adapter,
                'name' => $result->origin_name,
                'length' => $result->size,
                'url' => $result->file_name,
                'mime_type' => $result->mime_type,
                'extension' => $result->extension,
            ]);
            return success(message([
                'url' => $result->file_url,
                'name' => $result->origin_name,
                'size' => $result->size,
            ], 0, '上传成功'));
        } catch (\Exception $e) {
            return error(500, $e->getMessage(), [], $e->getFile() . '|' . $e->getLine());
        }
    }
}
