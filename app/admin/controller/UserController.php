<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\logic\UserLogic;

/**
 * 后台用户管理
 */
class UserController extends Backend
{


    public function __construct()
    {
        parent::__construct();
        $this->logic = new UserLogic();

    }

    /**
     * 添加或编辑
     * @return mixed
     */
    public function edit()
    {
        $data = request()->post();
        if (isset($data['id']) && $data['id'] == 1) {
            if ($this->userId != 1) {
                return error(-1, '非法操作');
            }
        }
        return success($this->logic::edit());
    }


}
