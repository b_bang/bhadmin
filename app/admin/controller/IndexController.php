<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\logic\LoginLogic;
use app\admin\logic\MenuLogic;
use app\admin\logic\UserLogic;

class IndexController extends Backend
{


    /**
     * 获取菜单列表
     * @return mixed
     * @since 2020/11/19
     */
    public function getMenuList()
    {
        return success(MenuLogic::getPermissionList($this->userId));
    }

    /**
     * 获取用户信息
     * @since 2020/11/19
     */
    public function getUserInfo()
    {
        return success(UserLogic::getUserInfo($this->userId));
    }

    /**
     * 修改用户信息
     * @since 2020/11/19
     */
    public function updateUserInfo()
    {
        return success(UserLogic::updateUserInfo($this->userId));
    }


    /**
     * 修改密码
     * @since 2020/11/19
     */
    public function updatePwd()
    {
        return success(UserLogic::updatePwd($this->userId));
    }


    /**
     * 退出
     * @since 2020/11/19
     */
    public function logout()
    {
        \Tinywan\Jwt\JwtToken::clear();
        LoginLogic::Logout();
        return success(message());
    }
}
