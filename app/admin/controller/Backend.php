<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\controller;
/**
 * 控制器基类
 */
class Backend
{


    /**
     * 用户登录id
     * @var int|mixed|string
     */
    protected $userId;

    /**
     * @var
     */
    protected $logic;


    public function __construct()
    {
        $this->userId = \Tinywan\Jwt\JwtToken::getCurrentId();
    }

    /**
     * 列表
     * @return \support\Response
     */
    public function index()
    {
        return success($this->logic::getList());
    }

    /**
     * 详情
     * @return \support\Response
     */
    public function info()
    {
        return success($this->logic::info());
    }

    /**
     * 新增或者编辑
     * @return \support\Response
     */
    public function edit()
    {
        return success($this->logic::edit());
    }


    /**
     * 删除
     * @return mixed
     */
    public function delete()
    {
        return success($this->logic::delete());
    }

    /**
     * 状态
     * @return mixed
     */
    public function status()
    {
        return success($this->logic::status());
    }

    /**
     * 导出
     * @return void
     */
    public function exportExcel()
    {
        return success($this->logic::exportExcel());
    }
}
