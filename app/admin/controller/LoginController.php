<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\logic\LoginLogic;
use Tinywan\Captcha\Captcha;

/**
 * 登录管理
 */
class LoginController
{

    protected $logic = null;

    public function __construct()
    {

        $this->logic = new LoginLogic();
    }

    /**
     * 获取验证码
     * @return array
     * @since 2020/11/14
     */
    public function captcha()
    {

        return success($this->logic::captcha());
    }

    /**
     * 登录
     * @return array
     * @since 2020/11/14
     */
    public function login()
    {
        return success($this->logic::login());
    }


}
