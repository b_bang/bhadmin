<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\logic\UserExtractLogic;

/**
 * 提现管理
 */
class ExtractController extends Backend
{
    public function __construct()
    {
        parent::__construct();
        $this->logic = new UserExtractLogic();
    }

    public function pass()
    {
        return success($this->logic::pass());
    }


    public function refuse()
    {
        return success($this->logic::refuse());
    }

}
