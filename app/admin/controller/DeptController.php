<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\logic\DeptLogic;

/**
 * 字典管理
 */
class DeptController extends Backend
{


    public function __construct()
    {
        parent::__construct();
        $this->logic = new DeptLogic();

    }

    /**
     * 获取部门列表
     * @since 2021/5/26
     */
    public function getDeptList()
    {
        return success(DeptLogic::getDeptList());
    }

    /**
     * 添加或编辑
     * @return mixed
     */
    public function edit()
    {
        return success(DeptLogic::edit());
    }

    /**
     * 删除
     * @return mixed
     */
    public function delete()
    {
        return success(DeptLogic::delete());
    }

    /**
     * 状态
     * @return mixed
     */
    public function status()
    {
        return success(DeptLogic::status());
    }
}
