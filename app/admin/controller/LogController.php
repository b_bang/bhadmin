<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\logic\LogLogic;
/**
 * 日志管理
 */
class LogController extends Backend
{

    public function loginLog()
    {
        return success(LogLogic::loginLogList());
    }

    public function actionLog()
    {
        return success(LogLogic::actionLogList());
    }

    /**
     * 删除
     * @return mixed
     */
    public function delete()
    {
        return success(LogLogic::delete());
    }
}
