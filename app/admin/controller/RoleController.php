<?php

namespace app\admin\controller;
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------

use app\admin\logic\RoleLogic;

/**
 * 角色管理
 */
class RoleController extends Backend
{

    public function __construct()
    {
        parent::__construct();
        $this->logic = new RoleLogic();
    }

    /**
     * 获取角色列表
     * @return mixed
     */
    public function getRoleList()
    {
        return success( $this->logic::getRoleList());
    }

    /**
     * 获取权限列表
     * @return mixed
     * @since 2020/11/20
     */
    public function getPermissionList()
    {
        return success( $this->logic::getPermissionList(request()->get('role_id')));
    }

    /**
     * 保存权限
     * @return mixed
     * @since 2020/11/20
     */
    public function savePermission()
    {
        return success( $this->logic::savePermission());
    }
}
