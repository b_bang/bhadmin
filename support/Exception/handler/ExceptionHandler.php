<?php

namespace support\Exception\handler;

use support\Exception\ApiException;
use Throwable;
use Webman\Http\Request;
use Webman\Http\Response;
use Tinywan\Jwt\Exception\JwtRefreshTokenExpiredException;
use Tinywan\Jwt\Exception\JwtTokenException;
use Tinywan\Jwt\Exception\JwtTokenExpiredException;

class ExceptionHandler extends \Webman\Exception\ExceptionHandler
{
    public function report(Throwable $e)
    {
        // TODO: Implement report() method.
    }

    public function render(Request $request, Throwable $e): Response
    {
        //处理token异常
        if ($e instanceof JwtTokenException || $e instanceof JwtTokenExpiredException || $e instanceof JwtRefreshTokenExpiredException) { //token校验错误
            return error(403, $e->getMessage());
        }
        //处理程序异常
        if ($e instanceof \ErrorException) {
            return error($e->getCode() ?? -1, $e->getMessage(), [], (config('app.debug') == true) ? ['file' => $e->getFile() . ':' . $e->getLine()] : []);
        }
        //处理接口异常
        if ($e instanceof ApiException || $e instanceof \PDOException) {
            return error($e->getCode() ?? -1, $e->getMessage(), [], (config('app.debug') == true) ? ['file' => $e->getFile() . ':' . $e->getLine()] : []);
        }
        // 处理其他异常
        return parent::render($request, $e);
    }
}
