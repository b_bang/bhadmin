<?php

namespace support\Service;

use support\Model\LevelModel;

class LevelService
{

    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $model = new LevelModel();
        $list = $model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }

    public static function getList($where, array $field = ['*'], array $order = [], array $with = [])
    {
        $model = new LevelModel();
        $list = $model->getAllList($where, $field, $order, $with);
        return $list;
    }

    /**
     * 获取职级列表
     * @return array
     * @since 2020/11/20
     */
    public static function getLevelList()
    {
        $model = new LevelModel();
        $list = $model->where([
            ['status', '=', 1],
            ['mark', '=', 1],
        ])->orderBy("sort", "asc")->get();
        return $list ? $list->toArray() : [];
    }

    public static function add($data)
    {
        $model = new LevelModel();
        return $model->insertOne($data);
    }

    public static function save($data, $id)
    {
        $model = new LevelModel();
        return $model->updateById($data, $id);
    }

    public static function delete($ids)
    {
        $model = new LevelModel();
        return $model->updateByIds(['mark' => 0], $ids);
    }
}