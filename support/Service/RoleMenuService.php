<?php

namespace support\Service;

use support\Model\RoleMenuModel;

class RoleMenuService
{

    public static function delete($role_id)
    {
        $model = new RoleMenuModel();
        return $model->where('role_id', $role_id)->delete();
    }

    public static function insertBatch($data)
    {
        $model = new RoleMenuModel();
        return $model->insertBatch($data);
    }
}