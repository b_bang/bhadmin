<?php

namespace support\Service;

use support\Model\FileModel;

class FileService
{
    public static function add($data)
    {
        $model = new FileModel();
        return $model->insertOne($data);
    }
}