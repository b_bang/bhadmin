<?php
// +----------------------------------------------------------------------
// | BhAdmin [ BhAdmin匠心打造，我们相信，每个伟大的软件都有一个伟大的故事 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2023 https://www.bhadmin.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed BhAdmin并不是自由软件，未经许可不能去掉BhAdmin相关版权
// +----------------------------------------------------------------------
// | Author: BAIHU  <admin@bhadmin.cn>
// +----------------------------------------------------------------------

namespace support\Service;
/**
 * 基础service
 */
class BaseService
{
    protected static $model = null;


    /**
     * 获取分页列表
     * @param array $where 查询条件
     * @param array $field 查询字段
     * @param array $order 排序
     * @param array $with 关联表
     * @param $limit        查询限制
     * @param $page         查询页数
     * @return mixed
     */
    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        return self::$model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }

    /**
     * 获取列表
     * @param array $where 查询条件
     * @param array $field 查询字段
     * @param array $order 排序
     * @param array $with 关联表
     * @return mixed
     */
    public static function getList(array $where, array $field = ['*'], array $order = [], array $with = [])
    {
        return self::$model->getAllList($where, $field, $order, $with);
    }

    /**
     * 获取详情
     * @param $id
     * @return array
     */
    public static function getInfo($id)
    {
        return self::$model->getInfoById($id);
    }

    /**
     * 获取详情
     * @param $id
     * @return array
     */
    public static function getInfoByWhere($where)
    {
        return self::$model->getInfoByWhere($where);
    }

    /**
     * 添加
     * @param $data
     * @return mixed
     */
    public static function add($data)
    {
        return self::$model->insertOne($data);
    }

    /**
     * 根据主键id更新
     * @param $data
     * @param $id
     * @return mixed
     */
    public static function save($data, $id)
    {
        return self::$model->updateById($data, $id);
    }

    /**
     * 根据主键ID删除
     * @param $ids
     * @return mixed
     */
    public static function delete($ids)
    {
        return self::$model->updateByIds(['mark' => 0], $ids);
    }
}
