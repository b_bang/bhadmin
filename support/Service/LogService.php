<?php

namespace support\Service;

use support\Model\ActionLogModel;

class LogService
{
    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $model = new ActionLogModel();
        $list = $model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }

    public static function delete($ids)
    {
        $model = new ActionLogModel();
        return $model->delByIds($ids);
    }
}