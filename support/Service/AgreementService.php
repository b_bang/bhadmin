<?php

namespace support\Service;

use support\Model\AgreementModel;

class AgreementService extends BaseService
{

    public function __construct()
    {
        self::$model = new AgreementModel();
    }

}
