<?php

namespace support\Service;

use support\Model\ConfigModel;

class ConfigService
{
    public static function getAllList(array $where, array $field = ['*'], array $order = [], array $with = [])
    {
        $model = new ConfigModel();
        $list = $model->getAllList($where, $field, $order, $with);
        return $list;
    }

    public static function getUserCountByWhere($where)
    {
        $model = new ConfigModel();
        return $model->getCountByWhere($where);
    }


    public static function save($data, $id)
    {
        $model = new ConfigModel();
        return $model->updateById($data, $id);
    }

    public static function add($data)
    {
        $model = new ConfigModel();
        return $model->insertOne($data);
    }

    public static function delete($ids)
    {
        $model = new ConfigModel();
        return $model->updateByIds(['mark' => 0], $ids);
    }
}