<?php

namespace support\Service;

use support\Model\MemberAccountModel;

class MemberAccountService
{
    public static function getAccountInfo($member_id)
    {
        $model = new MemberAccountModel();
        $info = $model->getInfoByWhere(['member_id' => $member_id]);
        if (!$info) {
            $data['member_id'] = $member_id;
            $data['balance'] = 0;
            $data['integral'] = 0;
            $model->insertOne($data);
        } else {
            $data['member_id'] = $member_id;
            $data['balance'] = $info['balance'];
            $data['integral'] = $info['integral'];
        }
        return $data;
    }
}