<?php

namespace support\Service;

use support\Model\MenuModel;
use support\Model\RoleMenuModel;
use support\Model\RoleModel;

class RoleService extends BaseService
{

    public function __construct()
    {
        self::$model = new RoleModel();
    }

    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $list = self::$model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }

    /**
     * 获取角色列表
     */
    public static function getRoleList()
    {
        $list = self::$model->where([
            ['status', '=', 1],
            ['mark', '=', 1],
        ])->orderBy("sort", "asc")->get();
        return $list ? $list->toArray() : [];
    }


    /**
     * 获取权限列表
     * @return array
     * @since 2020/11/20
     */
    public static function getPermissionList($role_id)
    {
        // 获取全部菜单
        $menuModel = new MenuModel();
        $menuList = $menuModel->where([
            ['status', '=', 1],
            ['mark', '=', 1],
        ])->orderBy("sort", "asc")->get();
        if (!$menuList) return [];

        $roleMenuModel = new RoleMenuModel();
        $roleMenuList = $roleMenuModel
            ->select(["menu_id"])
            ->where("role_id", '=', $role_id)
            ->get();
        $menuIdList = array_key_value($roleMenuList ? $roleMenuList->toArray() : [], "menu_id");
        foreach ($menuList as &$val) {
            if (in_array($val['id'], $menuIdList)) {
                $val['checked'] = true;
                $val['open'] = true;
            }
        }
        return $menuList;
    }


}
