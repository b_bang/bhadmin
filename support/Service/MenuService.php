<?php

namespace support\Service;

use support\Db;
use support\Model\MenuModel;

class MenuService extends BaseService
{


    public function __construct()
    {
        self::$model = new MenuModel();
    }

    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $model = new MenuModel();
        $list = $model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }

    public static function getList(array $where, array $field = ['*'], array $order = [], array $with = [])
    {
        $model = new MenuModel();
        $list = $model->getAllList($where, $field, $order, $with);
        return $list;
    }


    /**
     * 获取用户所有的权限节点
     * @param $userId
     * @return array
     */
    public static function getPermissionsList($userId)
    {
        new self();
        $model = self::$model;
        if ($userId == 1) {
            // 管理员拥有全部权限
            $permissionList = $model
                ->where("type", "=", 1)
                ->where("mark", "=", 1)
                ->distinct(true)
                ->select(["permission"])
                ->get();
            $list = empty($permissionList) ? [] : array_key_value($permissionList->toArray(), 'permission');
        } else {
            // 其他角色
            $permissionList = $model
                ->join('role_menu', 'role_menu.menu_id', '=', $model->getTable() . '.id')
                ->join('user_role', 'user_role.role_id', '=', 'role_menu.role_id')
                ->distinct(true)
                ->where('user_role.user_id', '=', $userId)
                ->where($model->getTable() . '.type', '=', 1)
                ->where($model->getTable() . '.status', '=', 1)
                ->where($model->getTable() . '.mark', '=', 1)
                ->select([$model->getTable() . '.permission'])
                ->get();
            $list = empty($permissionList) ? [] : array_key_value($permissionList->toArray(), 'permission');
        }
        return $list;
    }

    /**
     * 获取用户所有的权限节点
     * @param $userId
     * @return array
     */
    public static function getPermissionsPathList($userId)
    {

        $list = $permissionList = [];
        new  self();
        $model = self::$model;
        if ($userId == env('SUPER_ADMIN', 1)) {
            // 管理员拥有全部权限
            $permissionList = $model
                ->where("type", "=", 1)
                ->where("mark", "=", 1)
                ->distinct(true)
                ->select(["path"])
                ->get();
        } else {
            // 其他角色
            $permissionList = $model
                ->join('role_menu', 'role_menu.menu_id', '=', $model->getTable() . '.id')
                ->join('user_role', 'user_role.role_id', '=', 'role_menu.role_id')
                ->distinct(true)
                ->where('user_role.user_id', '=', $userId)
                ->where($model->getTable() . '.type', '=', 1)
                ->where($model->getTable() . '.status', '=', 1)
                ->where($model->getTable() . '.mark', '=', 1)
                ->select([$model->getTable() . '.path'])
                ->get();
        }
        $list = empty($permissionList) ? [] : array_key_value($permissionList->toArray(), 'path');
        return $list;
    }

    /**
     * 获取权限菜单
     * @param $userId 用户ID
     * @param $pid 上级ID
     * @return mixed
     */
    public static function getPermissionMenu($userId, $pid)
    {
        $menuModel = new MenuModel();
        $table = $menuModel->getTable();
        $menuList = $menuModel
            ->join('role_menu', 'role_menu.menu_id', '=', $table . '.id')
            ->join('user_role', 'user_role.role_id', '=', 'role_menu.role_id')
            ->distinct(true)
            ->where('user_role.user_id', '=', $userId)
            ->where($table . '.type', '=', 0)
            ->where($table . '.pid', '=', $pid)
            ->where($table . '.status', '=', 1)
            ->where($table . '.mark', '=', 1)
            ->orderBy($table . '.pid', 'ASC')
            ->orderBy($table . '.sort', 'ASC')
            ->select([$table . '.*'])
            ->get();
        if (!$menuList) return [];
        $menuList = $menuList->toArray();
        foreach ($menuList as &$val) {
            $childList = self::getPermissionMenu($userId, $val['id']);
            if (is_array($childList) && !empty($childList)) {
                $val['children'] = $childList;
            }
        }
        return $menuList;
    }

    /**
     * 获取子级菜单
     * @param $pid 上级ID
     * @return mixed
     * @since 2020/11/19
     */
    public static function getChilds($pid)
    {
        $model = new MenuModel();
        $map = [
            ['type', '=', 0],
            ['pid', '=', $pid],
            ['status', '=', 1],
            ['mark', '=', 1],
        ];
        $list = $model->where($map)->orderBy("sort", "asc")->get();
        if (!$list) return [];
        $list = $list->toArray();
        foreach ($list as &$val) {
            $val['children'] = self::getChilds($val['id']);
        }
        return $list;
    }


}
