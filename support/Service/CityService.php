<?php

namespace support\Service;

use support\Model\CityModel;

class CityService extends BaseService
{

    public function __construct()
    {
        self::$model = new CityModel();
    }


    public static function getList(array $where, array $field = ['*'], array $order = [], array $with = [])
    {
        $model = new CityModel();
        $list = $model->getAllList($where, $field, $order, $with);
        return $list;
    }

    public static function getUserCountByWhere($where)
    {
        $model = new CityModel();
        return $model->getCountByWhere($where);
    }


}
