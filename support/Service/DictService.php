<?php

namespace support\Service;

use support\Model\DictModel;

class DictService
{
    public static function getAllList(array $where, array $field = ['*'], array $order = [], array $with = [])
    {
        $model = new DictModel();
        $list = $model->getAllList($where, $field, $order, $with);
        return $list;
    }

    public static function getUserCountByWhere($where)
    {
        $model = new DictModel();
        return $model->getCountByWhere($where);
    }


    public static function save($data, $id)
    {
        $model = new DictModel();
        return $model->updateById($data, $id);
    }

    public static function add($data)
    {
        $model = new DictModel();
        return $model->insertOne($data);
    }

    public static function delete($ids)
    {
        $model = new DictModel();
        return $model->updateByIds(['mark' => 0], $ids);
    }
}