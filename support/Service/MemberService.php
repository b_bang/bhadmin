<?php

namespace support\Service;

use support\Model\MemberModel;

class MemberService
{
    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $model = new MemberModel();
        $list = $model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }

    public static function add($data)
    {
        $model = new MemberModel();
        return $model->insertOne($data);
    }

    public static function save($data, $id)
    {
        $model = new MemberModel();
        return $model->updateById($data, $id);
    }

    public static function delete($ids)
    {
        $model = new MemberModel();
        return $model->updateByIds(['mark' => 0], $ids);
    }

    public static function info($id)
    {
        $model = new MemberModel();
        return $model->getInfoById($id);
    }

}