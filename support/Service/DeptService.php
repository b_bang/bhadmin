<?php

namespace support\Service;

use support\Model\DeptModel;

class DeptService
{

    public static function getList(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $model = new DeptModel();
        $list = $model->getAllList($where, $field, $order, $with);
        return $list;
    }

    /**
     * 获取部门列表
     * @return array
     * @since 2021/5/26
     */
    public static function getDeptList()
    {
        $model = new DeptModel();
        $list = $model
            ->where("mark", "=", 1)
            ->get();
        return $list ? $list->toArray() : [];
    }

    public static function add($data)
    {
        $model = new DeptModel();
        return $model->insertOne($data);
    }

    public static function save($data, $id)
    {
        $model = new DeptModel();
        return $model->updateById($data, $id);
    }

    public static function delete($ids)
    {
        $model = new DeptModel();
        return $model->updateByIds(['mark' => 0], $ids);
    }
}