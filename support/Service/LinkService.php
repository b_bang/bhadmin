<?php

namespace support\Service;

use support\Model\LinkModel;

class LinkService extends BaseService
{

    public function __construct()
    {
        self::$model = new LinkModel();
    }

    public static function getUserCountByWhere($where)
    {
        return self::$model->getCountByWhere($where);
    }

}
