<?php

namespace support\Service;

use support\Model\UserModel;

class UserService extends BaseService
{


    public function __construct()
    {
        self::$model= new UserModel();
    }

    /**
     *
     * @param $userId
     * @return void
     */
    public static function getUserInfo($userId)
    {
        $model = new UserModel();
        $info = $model->getInfoById($userId);
        // 返回信息
        $result = [];
        $result['avatar'] = $info['avatar'];
        $result['realname'] = $info['realname'];
        $result['nickname'] = $info['nickname'];
        $result['gender'] = $info['gender'];
        $result['mobile'] = $info['mobile'];
        $result['email'] = $info['email'];
        $result['address'] = $info['address'];
        $result['intro'] = $info['intro'];
        $result['roles'] = [];
        $result['authorities'] = [];
        return $result;
    }


    public static function getUserCountByWhere($where)
    {
        $model = new UserModel();
        return $model->getCountByWhere($where);
    }

    public static function add($data)
    {
        $model = new UserModel();
        return $model->insertOne($data);
    }

    public static function save($data, $id)
    {
        $model = new UserModel();
        return $model->updateById($data, $id);
    }

    public static function delete($ids)
    {
        $model = new UserModel();
        return $model->updateByIds(['mark' => 0], $ids);
    }

}
