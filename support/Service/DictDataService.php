<?php

namespace support\Service;

use support\Model\DictDataModel;

class DictDataService
{
    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $model = new DictDataModel();
        $list = $model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }

    public static function getList(array $where, array $field = ['*'], array $order = [], array $with = [])
    {
        $model = new DictDataModel();
        $list = $model->getAllList($where, $field, $order, $with);
        return $list;
    }

    public static function getInfoByWhere(array $where)
    {
        $model = new DictDataModel();
        $info = $model->getInfoByWhere($where);
        return $info;
    }


    public static function getUserCountByWhere($where)
    {
        $model = new DictDataModel();
        return $model->getCountByWhere($where);
    }

    public static function save($data, $id)
    {
        $model = new DictDataModel();
        return $model->updateById($data, $id);
    }

    public static function add($data)
    {
        $model = new DictDataModel();
        return $model->insertOne($data);
    }


    public static function delete($ids)
    {
        $model = new DictDataModel();
        return $model->updateByIds(['mark' => 0], $ids);
    }
}