<?php

namespace support\Service;

use support\Model\PositionModel;

class PositionService
{

    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $model = new PositionModel();
        $list = $model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }

    /**
     * 获取岗位列表
     * @return array
     * @since 2020/11/20
     */
    public static function getPositionList()
    {
        $model = new PositionModel();
        $list = $model->where([
            ['status', '=', 1],
            ['mark', '=', 1],
        ])->orderBy("sort", "asc")->get();
        return $list ? $list->toArray() : [];
    }

    public static function add($data)
    {
        $model = new PositionModel();
        return $model->insertOne($data);
    }

    public static function save($data, $id)
    {
        $model = new PositionModel();
        return $model->updateById($data, $id);
    }

    public static function delete($ids)
    {
        $model = new PositionModel();
        return $model->updateByIds(['mark' => 0], $ids);
    }
}