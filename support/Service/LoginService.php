<?php

namespace support\Service;

use Tinywan\Captcha\Captcha;

class LoginService
{

    public static function captcha()
    {
        $res = Captcha::base64();
        $result['key'] = $res['key'];
        $result['captcha'] = $res['base64'];
        return $result;
    }
}