<?php

namespace support\Service;

use support\Model\ConfigDataModel;

class ConfigDataService extends BaseService
{
    public function __construct()
    {
        self::$model = new ConfigDataModel();
    }


    public static function getListPage(array $where, array $field = ['*'], array $order = [], array $with = [], $limit = 10, $page = 'page')
    {
        $model = new ConfigDataModel();
        $list = $model->getPaginateList($where, $field, $order, $with, $limit, $page);
        return $list;
    }


    public static function getUserCountByWhere($where)
    {
        $model = new ConfigDataModel();
        return $model->getCountByWhere($where);
    }


}
