<?php

namespace support\Model;

class MemberModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    public function memberAccount()
    {
        return $this->hasOne(MemberAccountModel::class, 'member_id', 'id');
    }

}