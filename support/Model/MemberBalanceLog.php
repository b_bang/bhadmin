<?php

namespace support\Model;

class MemberBalanceLog extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member_balance_log';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    protected $typeText = [
        'zh_CN' => [
            1 => '购买消耗',
            2 => '退款退回',
            3 => '后台增加',
            4 => '后台减少',
            5 => '充值',
            6 => '提现'
        ]
    ];

    public function getTypeText($type, $lang = 'zh_CN')
    {
        $text = isset($this->typeText[$lang]) ? $this->typeText[$lang] : $this->typeText['zh_CN'];
        return $text[$type] ?? '';
    }
}
