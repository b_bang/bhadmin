<?php

namespace support\Strategy\Filesystem\Adapter;

use Shopwwi\FilesystemOss\OssAdapter;
use support\Strategy\Filesystem\Contract\AdapterFactoryInterface;

class AliyunOssAdapterFactory implements AdapterFactoryInterface
{
    public function make(array $options)
    {
        return new OssAdapter($options);
    }
}
