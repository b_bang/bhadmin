<?php

namespace support\Strategy\Filesystem\Adapter;

use support\Strategy\Filesystem\Contract\AdapterFactoryInterface;
use Overtrue\Flysystem\Qiniu\QiniuAdapter;

class QiniuAdapterFactory implements AdapterFactoryInterface
{
    public function make(array $options)
    {
        return new QiniuAdapter($options['accessKey'], $options['secretKey'], $options['bucket'], $options['domain']);
    }
}
