<?php

namespace support\Strategy\Filesystem\Adapter;

use Overtrue\Flysystem\Cos\CosAdapter;
use support\Strategy\Filesystem\Contract\AdapterFactoryInterface;

class CosAdapterFactory implements AdapterFactoryInterface
{
    public function make(array $options)
    {
        return new CosAdapter($options);
    }
}
