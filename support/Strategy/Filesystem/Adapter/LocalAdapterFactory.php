<?php

namespace support\Strategy\Filesystem\Adapter;

use League\Flysystem\Local\LocalFilesystemAdapter;
use support\Strategy\Filesystem\Contract\AdapterFactoryInterface;

class LocalAdapterFactory implements AdapterFactoryInterface
{
    public function make(array $options)
    {
        return new LocalFilesystemAdapter($options['root']);
    }
}
