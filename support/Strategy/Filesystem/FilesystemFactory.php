<?php

namespace support\Strategy\Filesystem;

use support\Strategy\Filesystem\Adapter\AliyunOssAdapterFactory;
use support\Strategy\Filesystem\Adapter\CosAdapterFactory;
use support\Strategy\Filesystem\Adapter\LocalAdapterFactory;
use support\Strategy\Filesystem\Adapter\QiniuAdapterFactory;
use support\Strategy\Filesystem\Contract\AdapterFactoryInterface;
use League\Flysystem\Config;
use League\Flysystem\Filesystem;
use Psr\Container\ContainerInterface;
use support\Container;

class FilesystemFactory
{
    /**
     * @var ContainerInterface
     */
    protected static $_instance = null;

    /**
     * @return ContainerInterface
     */
    public static function instance()
    {
        return static::$_instance;
    }

    public static function get($adapterName): Filesystem
    {
        $options = [];
        if ($adapterName == 'local') {
            $options['storage']['local']['driver'] = LocalAdapterFactory::class;
            $options['storage']['local']['root'] = \public_path();
        }
        if ($adapterName == 'qcloud') {
            $options['storage']['qcloud']['driver'] = CosAdapterFactory::class;
            $config = getConfByType('store_tencent');
            $options['storage']['qcloud']['region'] = $config['tencent_endpoint'] ?? '';
            $options['storage']['qcloud']['app_id'] = $config['tencent_appid'] ?? '';
            $options['storage']['qcloud']['secret_id'] = $config['secret_id'] ?? '';
            $options['storage']['qcloud']['secret_key'] = $config['secret_key'] ?? '';
            $options['storage']['qcloud']['bucket'] = $config['tencent_bucket'] ?? '';

        }
        if ($adapterName == 'aliyun') {
            $options['storage']['aliyun']['driver'] = AliyunOssAdapterFactory::class;
            $config = getConfByType('store_oss');
            $options['storage']['aliyun']['accessId'] = $config['accesskey_id'] ?? '';
            $options['storage']['aliyun']['accessSecret'] = $config['accesskey_secret'] ?? '';
            $options['storage']['aliyun']['bucket'] = $config['bucket'] ?? '';
            $options['storage']['aliyun']['endpoint'] = $config['endpoint'] ?? '';
        }
        if ($adapterName == 'qiniu') {
            $options['storage']['qiniu']['driver'] = QiniuAdapterFactory::class;
            $config = getConfByType('store_qiniu');
            $options['storage']['qiniu']['accessKey'] = $config['accesskey'] ?? '';
            $options['storage']['qiniu']['secretKey'] = $config['secretkey'] ?? '';
            $options['storage']['qiniu']['bucket'] = $config['qiniu_bucket'] ?? '';
            $options['storage']['qiniu']['domain'] = $config['qiniu_endpoint'] ?? '';
        }
        $adapter = static::getAdapter($options, $adapterName);

        return new Filesystem($adapter);
    }

    public static function getAdapter($options, $adapterName)
    {
        if (!$options['storage'] || !$options['storage'][$adapterName]) {
            throw new \Exception("file configurations are missing {$adapterName} options");
        }
        /** @var AdapterFactoryInterface $driver */
        $driver = Container::get($options['storage'][$adapterName]['driver']);
        return $driver->make($options['storage'][$adapterName]);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return static::instance()->{$name}(... $arguments);
    }
}
