<?php

namespace support\Strategy\Filesystem\Contract;

use League\Flysystem\FilesystemAdapter;

interface AdapterFactoryInterface
{
    /**
     * @param array $options
     * @return mixed
     */
    public function make(array $options);
}