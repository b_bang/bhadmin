<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace support;

/**
 * Class Request
 * @package support
 */
class Request extends \Webman\Http\Request
{

    protected $api_data = null;

    /**
     * 设置内部 参数船传递(也可以在此处做统一解密)
     * @param $data
     * @return void
     */
    public function setApiData($data = [])
    {
        $this->api_data = $data;
    }

    /**
     * 内部获取参数
     * @param $name 参数名字
     * @param $default  参数值
     * @return void
     */
    public function getApiData($name = '', $default = null)
    {
        if (!$name && !$default) {
            return $this->api_data;
        }
        return $this->api_data[$name] ?? $default;
    }

}