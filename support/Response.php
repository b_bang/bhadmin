<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace support;

/**
 * Class Response
 * @package support
 */
class Response extends \Webman\Http\Response
{
    /**
     * 成功返回json
     * @param $return_data
     * @return \support\Response
     */
    public static function success($return_data)
    {
        if (!isset($return_data['code'])) {
            $return_data['code'] = 0;
        }
        if (!isset($return_data['msg'])) {
            $return_data['msg'] = 'success';
        }
        if (!isset($return_data['data'])) {
            $return_data['data'] = [];
        }
        return json($return_data);

    }
}